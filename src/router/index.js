import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginForm from "../components/LoginForm";
import SearchFilm from "../components/SearchFilm";
import RegisterForm from "../components/RegisterForm";
import store from "../store";

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/login',
            name: 'login',
            component: LoginForm


        },

        {
            path: '/register',
            name: 'register',
            component: RegisterForm
        },
        {
            path: '/search',
            name: 'search',
            component: SearchFilm,
            beforeEnter: (to, from, next) => {
                if (store.state.loggedIn) {
                    return next()
                } else
                    return next('login')
            }
        }
    ]
});


// router.beforeEach((to, from, next) => {
//   if (store.state.loggedIn) {
//     return next()
//   } else
//     return next('login')
// });

export default router;









